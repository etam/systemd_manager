// https://github.com/yoshuawuyts/html/issues/48
#![recursion_limit = "512"]

use clap::Parser;

mod backend;
mod config;
mod frontend;
mod systemctl;

use config::Args;

#[actix_web::main]
async fn main() -> anyhow::Result<()> {
    dotenvy::dotenv().ok();
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("info")).init();
    let args = Args::parse();

    backend::run_server(args).await
}

use actix_web::{get, http, post, web, App, HttpResponse, HttpServer, Result};
use anyhow::Context;
use serde::Deserialize;
use std::collections::BTreeMap;
use tokio_stream::{self as stream, StreamExt};

use crate::{
    config::{self, Config},
    frontend, systemctl,
};

#[derive(Deserialize, Clone, Debug)]
struct FormData {
    unit: String,
    action: systemctl::Action,
}

async fn make_unit_status(name: String) -> anyhow::Result<frontend::UnitStatus> {
    let status = systemctl::is_active(&name).await?;
    Ok(frontend::UnitStatus {
        unit: name,
        is_active: status.0,
        status_desc: status.1,
    })
}

async fn make_target_status(
    target: (String, Vec<String>),
) -> anyhow::Result<(frontend::UnitStatus, Vec<frontend::UnitStatus>)> {
    Ok((
        make_unit_status(target.0).await?,
        stream::iter(target.1)
            .then(make_unit_status)
            .collect::<anyhow::Result<Vec<_>>>()
            .await?,
    ))
}

async fn make_units_status(unit_desc: config::UnitsDesc) -> anyhow::Result<frontend::UnitsStatus> {
    Ok(match unit_desc {
        config::UnitsDesc::Service(service) => {
            frontend::UnitsStatus::Service(make_unit_status(service).await?)
        }
        config::UnitsDesc::Targets(targets) => frontend::UnitsStatus::Targets(BTreeMap::from_iter(
            stream::iter(targets)
                .then(make_target_status)
                .collect::<anyhow::Result<Vec<_>>>()
                .await?,
        )),
    })
}

async fn make_frontend_data_element(
    element: (String, Vec<config::UnitsDesc>),
) -> anyhow::Result<(String, Vec<frontend::UnitsStatus>)> {
    Ok((
        element.0,
        stream::iter(element.1)
            .then(make_units_status)
            .collect::<anyhow::Result<Vec<_>>>()
            .await?,
    ))
}

async fn make_frontend_data(config: &Config) -> anyhow::Result<frontend::Data> {
    Ok(frontend::Data(BTreeMap::from_iter(
        stream::iter(config.0.clone())
            .then(make_frontend_data_element)
            .collect::<anyhow::Result<Vec<_>>>()
            .await?,
    )))
}

async fn actix_make_page(config: &Config) -> Result<HttpResponse> {
    let data = make_frontend_data(config)
        .await
        .map_err(actix_web::error::ErrorInternalServerError)?;
    let page = frontend::make_page(data);
    Ok(HttpResponse::build(http::StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(page))
}

#[get("/")]
async fn root(config: web::Data<Config>) -> Result<HttpResponse> {
    actix_make_page(&config).await
}

fn is_unit_managed(unit: &str, config: &Config) -> bool {
    config.0.iter().any(|(_, units_descs)| {
        units_descs.iter().any(|units_desc| match units_desc {
            config::UnitsDesc::Service(service) => service == unit,
            config::UnitsDesc::Targets(targets) => targets.iter().any(|(target, services)| {
                target == unit || services.iter().any(|service| service == unit)
            }),
        })
    })
}

#[post("/service")]
async fn handle_service(
    form: web::Form<FormData>,
    config: web::Data<Config>,
) -> Result<HttpResponse> {
    log::info!("{:?} {}", form.action, form.unit);
    if !is_unit_managed(&form.unit, &config) {
        return Ok(
            HttpResponse::NotFound().body(format!("Error: unit \"{}\" not found", form.unit))
        );
    }
    systemctl::action(&form.action, &form.unit)
        .await
        .map_err(actix_web::error::ErrorInternalServerError)?;
    Ok(HttpResponse::SeeOther()
        .append_header((http::header::LOCATION, "/"))
        .finish())
}

pub async fn run_server(args: crate::config::Args) -> anyhow::Result<()> {
    let config: Config = serde_yaml::from_reader(std::io::BufReader::new(
        std::fs::File::open(args.config).context("Failed opening config file")?,
    ))
    .context("Failed to parse config")?;

    let data = web::Data::new(config);

    Ok(HttpServer::new(move || {
        App::new()
            .app_data(data.clone())
            .service(root)
            .service(handle_service)
    })
    .bind((args.bind_addr, args.port))?
    .run()
    .await?)
}

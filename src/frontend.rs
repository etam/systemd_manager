use std::collections::BTreeMap;

#[derive(Ord, PartialOrd, Eq, PartialEq)]
pub struct UnitStatus {
    pub unit: String,
    pub is_active: bool,
    pub status_desc: String,
}

pub enum UnitsStatus {
    Service(UnitStatus),
    Targets(BTreeMap<UnitStatus, Vec<UnitStatus>>),
}

pub struct Data(pub BTreeMap<String, Vec<UnitsStatus>>);

pub fn make_page(data: Data) -> String {
    html::root::Html::builder()
        .lang("en")
        .head(|head| {
            head.meta(|meta| meta.charset("utf-8"))
                .title(|title| title.text("Etam's game servers"))
                .meta(|meta| meta.http_equiv("refresh").content("10"))
                .style(|style| {
                    style.text(
                        r#"
body {
    font-size: 18px;
    line-height: 1.6;
    margin: 0 auto;
    max-width: 800px;
}

.pull-right {
    float: right;
}

form {
    display: inline;
}
"#,
                    )
                })
        })
        .body(|body| {
            body.heading_1(|heading_1| heading_1.text("Etam's game servers"));
            for (name, units) in data.0 {
                body.heading_2(|heading_2| heading_2.text(name))
                    .unordered_list(|list| {
                        for unit in units {
                            match unit {
                                UnitsStatus::Service(service) => {
                                    service_to_list_item(&service, list);
                                }
                                UnitsStatus::Targets(targets) => {
                                    targets_to_list_items(&targets, list);
                                }
                            }
                        }
                        list
                    });
            }
            body
        })
        .build()
        .to_string()
}

fn targets_to_list_items(
    targets: &BTreeMap<UnitStatus, Vec<UnitStatus>>,
    list: &mut html::text_content::builders::UnorderedListBuilder,
) {
    for (target, subservices) in targets {
        list.list_item(|li| {
            li.text(target.unit.clone())
                .division(|div| unit_status_control(target, div))
                .unordered_list(|sublist| {
                    for service in subservices {
                        service_to_list_item(service, sublist);
                    }
                    sublist
                })
        });
    }
}

fn service_to_list_item(
    service: &UnitStatus,
    list: &mut html::text_content::builders::UnorderedListBuilder,
) {
    list.list_item(|li| {
        li.text(service.unit.clone())
            .division(|div| unit_status_control(service, div))
    });
}

fn unit_status_control<'a>(
    status: &UnitStatus,
    div: &'a mut html::text_content::builders::DivisionBuilder,
) -> &'a mut html::text_content::builders::DivisionBuilder {
    div.class("pull-right")
        .span(|span| {
            span.style(format!(
                "color: {}",
                if status.is_active { "green" } else { "red" }
            ))
            .text("&#9679;")
        })
        .text(format!(" {} ", status.status_desc))
        .form(|form| {
            form.action("/service")
                .method("post")
                .input(|i| i.type_("hidden").name("unit").value(status.unit.clone()))
                .button(|b| {
                    b.type_("submit")
                        .name("action")
                        .value("Start")
                        .text("&#9654;")
                })
                .button(|b| {
                    b.type_("submit")
                        .name("action")
                        .value("Stop")
                        .text("&#9632;")
                })
                .button(|b| {
                    b.type_("submit")
                        .name("action")
                        .value("Restart")
                        .text("&#8634;")
                })
        })
}

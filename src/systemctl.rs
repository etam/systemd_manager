use anyhow::{bail, Result};
use serde::Deserialize;
use tokio::process::Command;

#[derive(Deserialize, Clone, Debug)]
pub enum Action {
    Start,
    Stop,
    Restart,
}

impl Action {
    fn as_arg(&self) -> &'static str {
        match self {
            Action::Start => "start",
            Action::Stop => "stop",
            Action::Restart => "restart",
        }
    }
}

pub async fn is_active(name: &str) -> Result<(bool, String)> {
    let output = Command::new("systemctl")
        .args(["--user", "is-active", name])
        .output()
        .await?;
    Ok((
        output.status.success(),
        std::str::from_utf8(&output.stdout)?.trim().to_owned(),
    ))
}

pub async fn action(action: &Action, name: &str) -> Result<()> {
    if !Command::new("systemctl")
        .args(["--user", action.as_arg(), name])
        .status()
        .await?
        .success()
    {
        bail!("systemctl failed");
    }

    Ok(())
}

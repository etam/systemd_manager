use clap::Parser;
use serde::Deserialize;
use std::{collections::BTreeMap, path::PathBuf};

#[derive(Parser)]
pub struct Args {
    #[arg(long, default_value = "0.0.0.0")]
    pub bind_addr: String,
    #[arg(long, default_value_t = 26235)]
    pub port: u16,
    #[arg(long, default_value = "config.yml")]
    pub config: PathBuf,
}

#[derive(Deserialize, Clone, Debug)]
#[serde(untagged)]
pub enum UnitsDesc {
    Service(String /*.service*/),
    Targets(BTreeMap<String /*.target*/, Vec<String /*.service*/>>),
}

#[derive(Deserialize, Clone, Debug)]
pub struct Config(pub BTreeMap<String /*name*/, Vec<UnitsDesc>>);
